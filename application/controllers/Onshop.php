<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onshop extends CI_Controller {

	
	public function index()
	{
		$this->load->view('layouts/layout');
	}
	public function home()
	{
		$this->load->view('onshop/home');
	}
	public function login() {
		$this->load->view('onshop/login');
	}
	public function sk() {
		$this->load->view('onshop/sk');
	}
	public function seleksismp() {
		$this->load->view('onshop/seleksismp');
	}
	public function lihatsmp() {
		$this->load->view('onshop/lihatsmp');
	}
	public function seleksisma() {
		$this->load->view('onshop/seleksisma');
	}
	public function lihatsma() {
		$this->load->view('onshop/lihatsma');
	}
	public function seleksismk() {
		$this->load->view('onshop/seleksismk');
	}
	public function lihatsmk() {
		$this->load->view('onshop/lihatsmk');
	}
	public function statistiksmp() {
		$this->load->view('onshop/statistiksmp');
	}
	public function statistiksma() {
		$this->load->view('onshop/statistiksma');
	}
	public function statistiksmk() {
		$this->load->view('onshop/statistiksmk');
	}
	public function pendaftaran() {
		$this->load->view('onshop/pendaftaran');
	}
}
