<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppdb extends CI_Controller {

	
	public function index()
	{
		$this->load->view('layouts/layout');
	}
	public function home()
	{
		$this->load->view('ppdb/home/home');
	}
	public function login() {
		$this->load->view('ppdb/login');
	}
	public function seleksismp() {
		$this->load->view('ppdb/seleksi/seleksismp');
	}
	public function lihatsmp() {
		$this->load->view('ppdb/seleksi/lihatsmp');
	}
	public function seleksisma() {
		$this->load->view('ppdb/seleksi/seleksisma');
	}
	public function lihatsma() {
		$this->load->view('ppdb/seleksi/lihatsma');
	}
	public function seleksismk() {
		$this->load->view('ppdb/seleksi/seleksismk');
	}
	public function lihatsmk() {
		$this->load->view('ppdb/seleksi/lihatsmk');
	}
	public function statistiksmp() {
		$this->load->view('ppdb/statistik/statistiksmp');
	}
	public function statistiksma() {
		$this->load->view('ppdb/statistik/statistiksma');
	}
	public function statistiksmk() {
		$this->load->view('ppdb/statistik/statistiksmk');
	}
	public function pendaftaran() {
		$this->load->view('ppdb/pendaftaran/pendaftaran');
	}
	public function sdmasuk() {
		$this->load->view('ppdb/pendaftaran/sdmasuk');
	}
	public function daftarsmp() {
		$this->load->view('ppdb/pendaftaran/daftarsmp');
	}
	public function faq() {
		$this->load->view('ppdb/home/faq');
	}
}
