<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<div class="home-image">
  <a><img src="<?php echo base_url(); ?>public/img/home/depan.png"  alt="" /></a>
</div>
<div class="col-sm-2"></div>
<div class="col-sm-8">
 	<div class="alur">
    	<h2 class="title text-center">Alur</h2>
  	</div>
</div>
<div class="col-sm-2"></div>
<div class="col-sm-3"></div>
<div class="col-sm-6">
	<div class="alur-image">
		<a><img src="<?php echo base_url(); ?>public/img/home/alur.jpg"  width="600" height="800" alt="" /></a>
	</div>
</div>
<div class="col-sm-3"></div>
<?php endsection() ?>
<?php getview('layouts/layout') ?>