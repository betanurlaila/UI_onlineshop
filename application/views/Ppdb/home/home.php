<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>

<div class="col-sm-2"></div>
<div class="col-sm-8">
 	<div class="log-in">
    <h2 class="title text-center">Log In Siswa</h2>
  </div>
</div>
<div class="col-sm-2"></div>
<div class="row">
	<div class="student-login">
  		<div class="col-sm-3"></div>
 		  <div class="col-sm-3">
  			<div class="thumbnail">
    			<div class="sd">
      				<center><i class="fa fa-user"></i></center>
      			<div class="caption">
       				<h3>Log In SD/MI</h3>
        			<p>Log In PPDB ONLINE 2016 untuk siswa SD/MI</p>
        			<div class="btn-login">
          				<a class="btn btn-danger" href="#" data-toggle="modal" data-target="#sksd" >LogIn</a>
        			</div>
              <div class="modal fade" id="sksd" tabindex="-1" role="dialog" aria-labelledby="sksd" allign="center">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Syarat & Ketentuan PPDB ONLINE 2016</h4>
                    </div>
                    <div class="modal-body">
                      <p>1.&nbsp;&nbsp;Peserta merupakan siswa SD/MI yang sudah dinyatakan lulus oleh pihak sekolah</p>
                      <p>2.&nbsp;&nbsp;Peserta harus sudah tedaftar diwebsite <a href="#">ppdb.tulungagung.net</a></p>
                      <p>3.&nbsp;&nbsp;Apabila anda adalah siswa lulusan tahun 2014/2015 atau berasal dari luar 
                      <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kabupaten tulungagung anda bisa mendaftar di 
                      <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">ppdb.tulungagung.net/daftar/daftarsltp</a></p>
                    </div>
                    <div class="modal-footer">
                      <a href="<?= base_url('ppdb/sdmasuk') ?>"><button type="button" class="btn btn-success">OK</button></a>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                  </div>
                </div>
              </div>
      			</div>
    		  </div>
    	  </div>
  	  </div>
  	  <div class="col-sm-3">
    	  <div class="thumbnail">
    		  <div class="smp">
      			<center><i class="fa fa-user"></i></center>
      			<div class="caption">
        			<h3>Log In SMP/MTs</h3>
        			<p>Log In PPDB ONLINE 2016 untuk siswa SMP/MTs</p>
        			<div class="btn-login">
          				<a class="btn btn-warning" href="#" data-toggle="modal" data-target="#sksmp">LogIn</a>
        			</div>
              <div class="modal fade" id="sksmp" tabindex="-1" role="dialog" aria-labelledby="sksmp" allign="center">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Syarat & Ketentuan PPDB ONLINE 2016</h4>
                    </div>
                    <div class="modal-body">
                      <p>1.&nbsp;&nbsp;Peserta merupakan siswa SMP/MTs yang sudah dinyatakan lulus oleh pihak sekolah</p>
                      <p>2.&nbsp;&nbsp;Peserta harus sudah tedaftar diwebsite <a href="#">ppdb.tulungagung.net</a></p>
                      <p>3.&nbsp;&nbsp;Apabila anda adalah siswa lulusan tahun 2014/2015 atau berasal dari luar 
                      <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kabupaten tulungagung anda bisa mendaftar di 
                      <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">ppdb.tulungagung.net/daftar/daftarsltp</a></p>
                    </div>
                    <div class="modal-footer">
                      <a href="#"><button type="button" class="btn btn-success">OK</button></a>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                  </div>
                </div>
              </div>
      			</div>
    		  </div>
    	  </div>
  	  </div>
  	  <div class="col-sm-3"></div>
  </div>
</div>
<div class="col-sm-2"></div>
<div class="col-sm-8">
  <div class="log-in">
    <h2 class="title text-center">Log In Operator</h2>
  </div>
</div>
<div class="col-sm-2"></div>
<div class="row">
  <div class="operator-login">
      <div class="col-sm-1"></div>
      <div class="col-sm-3">
        <div class="thumbnail">
          <div class="oper-sd">
              <center><i class="fa fa-university"></i></center>
            <div class="caption">
              <h3>Log In Operator <br> SD/MI</h3>
              <p>Log In PPDB ONLINE 2016 untuk operator sekolah SD/MI</p>
              <div class="btn-login">
                  <a class="btn btn-danger" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn SD/MI">LogIn</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumbnail">
          <div class="oper-smp">
            <center><i class="fa fa-university"></i></center>
            <div class="caption">
              <h3>Log In Operator SMP/MTs</h3>
              <p>Log In PPDB ONLINE 2016 untuk opearator sekolah SMP/MTs</p>
              <div class="btn-login">
                  <a class="btn btn-warning" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn SMP/MTs">LogIn</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumbnail">
          <div class="oper-sma">
            <center><i class="fa fa-university"></i></center>
            <div class="caption">
              <h3>Log In Operator SMA/SMK</h3>
              <p>Log In PPDB ONLINE 2016 untuk opearator sekolah SMA/SMK</p>
              <div class="btn-login">
                  <a class="btn btn-success" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn SMA/SMK">LogIn</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-2"></div>
  </div>
</div>
<div class="col-sm-2"></div>
<div class="col-sm-8">
  <div class="alur">
    <h2 class="title text-center">Alur</h2>
  </div>
</div>
<div class="col-sm-2"></div>
<div class="col-sm-3"></div>
<div class="col-sm-6">
  <div class="alur-image">
    <a><img src="<?php echo base_url(); ?>public/img/home/alur.jpg"  width="600" height="800" alt="" /></a>
  </div>
</div>
<div class="col-sm-3"></div>
<?php endsection() ?>
<?php getview('layouts/layout') ?>