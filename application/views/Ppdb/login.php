<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
  <div class="col-sm-6">
    <div class="registration-student">
      <h2 class="title text-center">Log In Siswa</h2>
    </div>
  </div>
  <div class="col-md-10">
    <div class="col-sm-5">
    <div class="sd">
      <h4><i class="fa fa-user"></i>&nbsp; &nbsp; &nbsp; &nbsp;Log In Siswa SD/MI</h4>
      <h5>Log In PPDB ONLINE 2016</h5>
      <h5>Untuk Siswa SD/MI</h5> 
      <div class="btn-sign" role="group">
        <i class="fa fa-sign-in"> <a class="btn btn-success" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn">Klik</i></a>
      </div>
      </div>
    </div>
    <div class="col-sm-5">
      <h4><i class="fa fa-user"></i>&nbsp; &nbsp; &nbsp; &nbsp;Log In Siswa SMP/MTs</h4>
      <h5>Log In PPDB ONLINE 2016</h5>
      <h5>Untuk Siswa SMP/MTs</h5> 
      <div class="btn-sign" role="group">
        <i class="fa fa-sign-in"> <a class="btn btn-info" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn">Klik</i></a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="registration-school">
      <h2 class="title text-center">Log In Sekolah</h2>
    </div>
  </div>
  <div class="col-md-10">
    <div class="col-sm-5">
      <h4><i class="fa fa-university"></i>&nbsp; &nbsp; &nbsp; &nbsp;Log In Sekolah SD/MI</h4>
      <h5>Log In PPDB ONLINE 2016</h5>
      <h5>Untuk Siswa SD/MI</h5> 
      <div class="btn-sign-school" role="group">
        <i class="fa fa-sign-in"> <a class="btn btn-danger" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn">Klik</i></a>
      </div>
    </div>
    <div class="col-sm-5">
      <h4><i class="fa fa-university"></i>&nbsp; &nbsp; &nbsp; &nbsp;Log In Sekolah SMP/MTs</h4>
      <h5>Log In PPDB ONLINE 2016</h5>
      <h5>Untuk Siswa SMP/MTs</h5> 
      <div class="btn-sign-school" role="group">
        <i class="fa fa-sign-in"> <a class="btn btn-warning" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn">Klik</i></a>
      </div>
    </div>
  </div>
  <div class="school">
  <div class="col-md-10">
    <div class="col-sm-5">
      <h4><i class="fa fa-university"></i>&nbsp; &nbsp; &nbsp; &nbsp;Log In Sekolah SMA</h4>
      <h5>Log In PPDB ONLINE 2016</h5>
      <h5>Untuk Sekolah SMA</h5> 
      <div class="btn-sign-school" role="group">
        <i class="fa fa-sign-in"> <a class="btn btn-primary" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn">Klik</i></a>
      </div>
    </div>
    <div class="col-sm-5">
      <h4><i class="fa fa-university"></i>&nbsp; &nbsp; &nbsp; &nbsp;Log In Sekolah SMK</h4>
      <h5>Log In PPDB ONLINE 2016</h5>
      <h5>Untuk Sekolah SMK</h5> 
      <div class="btn-sign-school" role="group">
        <i class="fa fa-sign-in"> <a class="btn btn-default" href="#" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn">Klik</i></a>
      </div>
    </div>
  </div>
  </div>
<?php endsection() ?>
<?php getview('layouts/layout') ?>