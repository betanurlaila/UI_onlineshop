<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Home</a></li>
  <li><a>Login SD/MI</a></li>
  <li><a>Login</a></li>
  <li><a>Daftar SMP</a></li>
</ol>
<div class="col-sm-3"></div>
<div class="col-sm-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <center>Daftar SMP</center>
    </div>
    <div class="panel-body">
      <form class="form-horizontal">
        <div class="form-group">
          <label for="no" class="col-sm-4 control-label-left">No Ujian</label>
          <div class="col-sm-8">
            <input type="email" class="form-control" id="no" placeholder="">
          </div>
        </div>
        <div class="form-group">
          <label for="nama" class="col-sm-4 control-label-left">Nama</label>
          <div class="col-sm-8">
            <input type="name" class="form-control" id="nama" placeholder="">
          </div>
        </div>
        <div class="form-group">
          <label for="birth" class="col-sm-4 control-label-left">Tempat,Tanggal Lahir</label>
          <div class="col-sm-8">
            <input type="name" class="form-control" id="birth" placeholder="">
          </div>
        </div>
        <div class="form-group">
          <label for="gender" class="col-sm-4 control-label-left">Jenis Kelamin</label>
          <div class="col-sm-8">
            <input type="name" class="form-control" id="gender" placeholder="">
          </div>
        </div>
        <div class="form-group">
          <label for="address" class="col-sm-4 control-label-left">Alamat</label>
          <div class="col-sm-8">
            <input type="name" class="form-control" id="address" placeholder="">
          </div>
        </div>
        <div class="form-group">
          <label for="school" class="col-sm-4 control-label-left">Asal Sekolah</label>
          <div class="col-sm-8">
            <input type="name" class="form-control" id="school" placeholder="">
          </div>
        </div>
      </form>
      <br>
      <br>
      <div class="col-sm-3"></div>
      <div class="col-sm-6">
        <table class="table table-bordered table-striped table-responsive">
          <tr>
            <td><center>#</center></td>
            <td><center>Mata Pelajaran</center></td>
            <td><center>NUN</center></td>
          </tr>
          <tr>
            <td><center>1</center></td>
            <td><center>Bhs.Indonesia</center></td>
            <td><center>9,50</center></td>
          </tr>
          <tr>
            <td><center>2</center></td>
            <td><center>Matematika</center></td>
            <td><center>10,00</center></td>
          </tr>
          <tr>
            <td><center>3</center></td>
            <td><center>IPA</center></td>
            <td><center>9,00</center></td>
          </tr>
        </table>
      <div class="col-sm-3"></div>
      <div class="form-group">
        <div class="col-sm-offset-9 col-sm-3">
          <button type="submit" class="btn btn-info" data-toggle="modal" data-target="#daftarsmp"><i class="fa fa-edit"></i> Daftar</button>
        </div>
        <div class="modal fade" id="daftarsmp" tabindex="-1" role="dialog" aria-labelledby="daftarsmp" allign="center">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <p>!!&nbsp;&nbsp;Pastikan data yang anda masukkan telah benar dan sesuai</p>
                <p>!!&nbsp;&nbsp;Data yang sudah terdaftar tidak dapat dirubah</p>
                <p>!!&nbsp;&nbsp;Pastikan pilihan sekolah yang tercantum telah sesuai dengan keinginan anda</p>
              </div>
              <div class="modal-footer">
                <a href="#"><button type="button" class="btn btn-success">OK</button></a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-3"></div>

<?php endsection() ?>
<?php getview('layouts/layout') ?>