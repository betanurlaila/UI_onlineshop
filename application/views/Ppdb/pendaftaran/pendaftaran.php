<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<div class="col-sm-12">
  <div class="daftar">
      <h2 class="title text-center">Pendaftaran</h2>
    </div>
</div>
<div class="row">
  <div class="col-sm-1"></div>
  <div class="col-sm-3">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>public/img/home/seragam_smp.png"  width="300" height="400" alt="">
      <div class="caption">
        <h3>Memilih ke SMP</h3>
        <p>Daftar PPDB ONLINE 2016 untuk siswa SD/MI</p>
        <div class="btn-daftar">
          <a class="btn btn-danger" href="<?= base_url('ppdb/daftarsmp') ?>" data-toggle="tooltip" data-placement="bottom" title="Untuk Daftar SMP">Daftar</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>public/img/home/seragam_sma.png"  width="300" height="400" alt="">
      <div class="caption">
        <h3>Memilih ke SMA</h3>
        <p>Daftar PPDB ONLINE 2016 untuk siswa SMP/MTs</p>
        <div class="btn-daftar">
          <a class="btn btn-warning" href="<?= base_url('ppdb/login') ?>" data-toggle="tooltip" data-placement="bottom" title="Untuk Daftar SMA">Daftar</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>public/img/home/seragam_smk.png"  width="300" height="400" alt="">
      <div class="caption">
        <h3>Memilih ke SMK</h3>
        <p>Daftar PPDB ONLINE 2016 untuk siswa SMP/MTs</p>
        <div class="btn-daftar">
          <a class="btn btn-success" href="<?= base_url('ppdb/login') ?>" data-toggle="tooltip" data-placement="bottom" title="Untuk Daftar SMK">Daftar</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-2"></div>
</div>
<?php endsection() ?>
<?php getview('layouts/layout') ?>