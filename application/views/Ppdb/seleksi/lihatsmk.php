<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Seleksi</a></li>
  <li><a>Seleksi SMK</a></li>
  <li><a>SMKN 1 Boyolangu (Keuangan)</a></li>
</ol>
<table class="table table-bordered table-striped table-hover table-responsive">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Siswa</center></td>
  	<td><center>Asal Sekolah</center></td>
    <td><center>Bobot</center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>Ana Ani</center></td>
  	<td><center>SMPN 3 Tulungagung</center></td>
  	<td><center>92,00</center></td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td><center>Ana Ani</center></td>
    <td><center>SMPN 3 Tulungagung</center></td>
    <td><center>92,00</center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>