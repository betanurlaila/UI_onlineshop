<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Seleksi</a></li>
  <li><a>Seleksi SMP</a></li>
  <li><a>SMPN 1 Tulungagung</a></li>
</ol>
<table class="table table-bordered table-striped table-hover table-responsive">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Siswa</center></td>
  	<td><center>Asal Sekolah</center></td>
    <td><center>Bhs.Indonesia</center></td>
    <td><center>Matematika</center></td>
    <td><center>IPA</center></td>
  	<td><center>NUN</center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>Ana Ani</center></td>
  	<td><center>SDN 2 Botoran</center></td>
  	<td><center>9,5</center></td>
  	<td><center>10,0</center></td>
    <td><center>9,0</center></td>
    <td><center>28,5</center></td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td><center>Ana Ani</center></td>
    <td><center>SDN 2 Botoran</center></td>
    <td><center>9,5</center></td>
    <td><center>10,0</center></td>
    <td><center>9,0</center></td>
    <td><center>28,5</center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>