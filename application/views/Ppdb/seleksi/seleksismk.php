<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Seleksi</a></li>
  <li><a>Seleksi SMK</a></li>
</ol>
<table class="table table-bordered table-striped table-hover">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Sekolah</center></td>
  	<td><center>Pagu Terpenuhi</center></td>
  	<td><center>Pagu Tersisa</center></td>
  	<td><center>Bobot Terendah</center></td>
  	<td><center>Bobot Tertinggi</center></td>
  	<td><center></center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>SMKN 1 Boyolangu (Keuangan)</center></td>
  	<td><center>146</center></td>
  	<td><center>14</center></td>
  	<td><center>97,10</center></td>
  	<td><center>90,20</center></td>
  	<td><center><a class="btn btn-info" href="<?= base_url('ppdb/lihatsmk') ?>"><i class="fa fa-eye"></i> Lihat</a></center></td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td><center>SMKN 1 Boyolangu (Tata Niaga)</center></td>
    <td><center>146</center></td>
    <td><center>14</center></td>
    <td><center>97,10</center></td>
    <td><center>90,20</center></td>
    <td><center><a class="btn btn-info" href="<?= base_url('ppdb/lihatsmk') ?>"><i class="fa fa-eye"></i> Lihat</a></center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>