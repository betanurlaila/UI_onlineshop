<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Statistik</a></li>
  <li><a href="<?= base_url('ppdb/statistiksma') ?>">Pagu SMA</a></li>
</ol>
<table class="table table-bordered table-striped table-hover">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Sekolah</center></td>
    <td><center>Jumlah Pagu</center></td>
  	<td><center>Pagu Terpenuhi</center></td>
  	<td><center>Pagu Tersisa</center></td>
  	<td><center>Nilai Terendah</center></td>
  	<td><center>Nilai Tertinggi</center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>SMAN 1 Kedungwaru</center></td>
  	<td><center>400</center></td>
  	<td><center>198</center></td>
  	<td><center>202</center></td>
  	<td><center>39,50</center></td>
    <td><center>37,75</center></td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td><center>SMAN 1 Boyolangu</center></td>
    <td><center>400</center></td>
    <td><center>176</center></td>
    <td><center>224</center></td>
    <td><center>37,80</center></td>
    <td><center>34,80</center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>