<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Statistik</a></li>
  <li><a href="<?= base_url('ppdb/statistiksmp') ?>">Pagu SMP</a></li>
</ol>
<table class="table table-bordered table-striped table-hover">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Sekolah</center></td>
    <td><center>Jumlah Pagu</center></td>
  	<td><center>Pagu Terpenuhi</center></td>
  	<td><center>Pagu Tersisa</center></td>
  	<td><center>Nilai Terendah</center></td>
  	<td><center>Nilai Tertinggi</center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>SMPN 1 Tulungagung</center></td>
  	<td><center>400</center></td>
  	<td><center>289</center></td>
  	<td><center>111</center></td>
  	<td><center>28,10</center></td>
    <td><center>29,85</center></td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td><center>SMPN 1 Tulungagung</center></td>
    <td><center>400</center></td>
    <td><center>289</center></td>
    <td><center>111</center></td>
    <td><center>28,10</center></td>
    <td><center>29,85</center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>