<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PPDB ONLINE 2016</title>
    <link href="<?= base_url('public/plugins/css/bootstrap.min.css" rel="stylesheet') ?>">
    <link href="<?= base_url('public/plugins/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet') ?>">
    <link href="<?= base_url('public/plugins/css/prettyPhoto.css" rel="stylesheet') ?>">
    <link href="<?= base_url('public/plugins/css/price-range.css" rel="stylesheet') ?>">
    <link href="<?= base_url('public/plugins/css/animate.css" rel="stylesheet') ?>">
    <link href="<?= base_url('public/plugins/css/main.css" rel="stylesheet') ?>">
    <link href="<?= base_url('public/plugins/css/responsive.css" rel="stylesheet') ?>">
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
  <div class="first-logo">
    <a><img src="<?php echo base_url(); ?>public/img/home/ppdb.png" width="150" height="50" alt="" /></a>
    <div class="btn-group" role="group">
      <i class="fa fa-sign-in"> <a class="btn btn-default" href="<?= base_url('ppdb/login') ?>" data-toggle="tooltip" data-placement="bottom" title="Untuk LogIn">Klik</i></a>
    </div>
  </div>
<nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="<?= base_url('ppdb/home/home') ?>"><i class="fa fa-home"></i> Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bullhorn"></i> Informasi<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-edit"></i> Cara Daftar SMP</a></li>
                <li><a href="#"><i class="fa fa-edit"></i> Cara Daftar SMA</a></li>
                <li><a href="#"><i class="fa fa-edit"></i> Cara Daftar SMK</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-line-chart"></i> Statistik<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?= base_url('ppdb/statistiksmp') ?>"><i class="fa fa-users"></i> Pagu SMP</a></li>
                <li><a href="<?= base_url('ppdb/statistiksma') ?>"><i class="fa fa-users"></i> Pagu SMA</a></li>
                <li><a href="<?= base_url('ppdb/statistiksmk') ?>"><i class="fa fa-users"></i> Pagu SMK</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-trophy"></i> Seleksi<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?= base_url('ppdb/seleksismp') ?>"><i class="fa fa-trophy"></i> Seleksi SMP</a></li>
                <li><a href="<?= base_url('ppdb/seleksisma') ?>"><i class="fa fa-trophy"></i> Seleksi SMA</a></li>
                <li><a href="<?= base_url('ppdb/seleksismk') ?>"><i class="fa fa-trophy"></i> Seleksi SMK</a></li>
              </ul>
            </li>
          <li><a href="<?= base_url('ppdb/pendaftaran/pendaftaran') ?>"><i class="fa fa-edit"></i> Pendaftaran</a></li>
          <li><a href="<?= base_url('ppdb/faq') ?>"><i class="fa fa-envelope"></i> FAQ</a></li>
        </ul>
        <form class="navbar-form navbar-left" role="search">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Cari No Ujian">
          </div>
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
      </div>
    </div>
  </nav>
  <img src="<?php echo base_url(); ?>public/img/home/coba.jpg" width="1500" height="300" alt="" class="home-image" />
</div>
  
  <div class="content-wrapper">
    <section class="content">
      <?php render('content') ?>
    </section>
  </div>   
    <script src="<?= base_url('public/dist/js/jquery.js') ?>"></script>
    <script src="<?= base_url('public/dist/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('public/dist/js/jquery.scrollUp.min.js') ?>"></script>
    <script src="<?= base_url('public/dist/js/price-range.js') ?>"></script>
    <script src="<?= base_url('public/dist/js/jquery.prettyPhoto.js') ?>"></script>
    <script src="<?= base_url('public/dist/js/main.js') ?>"></script>
    <script src="<?= base_url('public/dist/js/app.min.js') ?>"></script>
</body>
</html>